package com.ivandelgadog.hcvet.mvp;

import android.app.Activity;

public interface LoginMVP {

    interface Model{

        void validateCredentials(String email, String password, ValidateCredentialsCallback callback);

        interface ValidateCredentialsCallback {
            void onSuccess();
            void onFailure(String error);
        }

    }

    //Cuando hago alguna acción en la pantalla (Presionar botones)
    interface Presenter{
        void loginWithEmail();
        void registerNewUser();
    }

    interface View{

        Activity getActivity();

        LoginInfo getLoginInfo();

        void showEmailError(String error); //Si está bn escrito
        void showPasswordError(String error);  //Si tiene cierta cantidad de caracteres
        void showGeneralError(String error); //El usuario no existe en la BD

        void clearData(); //Cuando se pulsa el botón y no han digitado nada y a la vez borra

        void openNewHcActivity();

        //Esta es de nosotros
        void openNewUserActivity();

    }

    class LoginInfo{
        private String email;
        private String password;

        public LoginInfo(String email, String password){
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
