package com.ivandelgadog.hcvet.mvp;

public interface NewHcMVP {

    interface Model{
        boolean registerDataNewHc(String dateHc, String patientIdHc,
                                    String patientNameHc, String patientSpecieHc,
                                    String patientAgeHc, String patientRaceHc,
                                    String ownerNameHc, String ownerEmailHc);
    }

    //Cuando hago alguna acción en la pantalla (Presionar botones)
    interface Presenter{
        void registerNewHc();
        void cleanDataNewHc();
    }

    interface View{

        NewHcMVP.NewHcInfo getNewHcInfo();

        void showpatientIdHcError(String error);
        void showpatientNameHcError(String error);
        void showpatientSpecieError(String error);
        void showpatientAgeError(String error);
        void showpatientRaceHcError(String error);
        void showownerNameHcError(String error);
        void showownerEmailHcError(String error);
        void showGeneralError(String error);

        //Pendiente para reutilizar como el de LoginActivity
        void clearData(); //Cuando se pulsa el botón y no han digitado nada y a la vez borra

        //void openLoginActivity(); //Una vez se ingresan los datos, vuelve a la pantalla de Login OJO

    }

    class NewHcInfo{
        private String dateHc;
        private String patientIdHc;
        private String patientNameHc;
        private String patientSpecieHc;
        private String patientAgeHc;
        private String patientRaceHc;
        private String ownerNameHc;
        private String ownerEmailHc;

        public NewHcInfo(String dateHc, String patientIdHc,
                           String patientNameHc, String patientSpecieHc,
                           String patientAgeHc, String patientRaceHc,
                           String ownerNameHc, String ownerEmailHc){
            this.dateHc = dateHc;
            this.patientIdHc = patientIdHc;
            this.patientNameHc = patientNameHc;
            this.patientSpecieHc = patientSpecieHc;
            this.patientAgeHc = patientAgeHc;
            this.patientRaceHc = patientRaceHc;
            this.ownerNameHc = ownerNameHc;
            this.ownerEmailHc = ownerEmailHc;
        }

        public String getDateHc() {
            return dateHc;
        }

        public String getPatientIdHc() {
            return patientIdHc;
        }

        public String getPatientNameHc() {
            return patientNameHc;
        }

        public String getPatientSpecieHc() {
            return patientSpecieHc;
        }

        public String getPatientAgeHc() {
            return patientAgeHc;
        }

        public String getPatientRaceHc() {
            return patientRaceHc;
        }

        public String getOwnerNameHc() {
            return ownerNameHc;
        }

        public String getOwnerEmailHc() {
            return ownerEmailHc;
        }
    }

}
