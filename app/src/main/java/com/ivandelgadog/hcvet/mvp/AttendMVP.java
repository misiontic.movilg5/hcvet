package com.ivandelgadog.hcvet.mvp;

public interface AttendMVP {

    interface Model{
        boolean searchDataAttend(String attendId);
        boolean registerDataAttend(String attendId, String attendDate, String attendDescription);
    }

    //Cuando hago alguna acción en la pantalla (Presionar botones)
    interface Presenter{
        void searchAttend();
        void registerAttend();
        void cleanDataAttend();
    }

    interface View{

        AttendMVP.AttendInfo getAttendInfo();

        void showAttendIdError(String error);
        void showAttendDescriptionError(String error);
        void showGeneralError(String error);

        //Pendiente para reutilizar como el de LoginActivity
        void clearData(); //Cuando se pulsa el botón y no han digitado nada y a la vez borra

    }

    class AttendInfo{
        private String attendId;
        private String attendDate;
        private String attendDescription;

        public AttendInfo(String attendId, String attendDate, String attendDescription){
            this.attendId = attendId;
            this.attendDate = attendDate;
            this.attendDescription = attendDescription;
        }

        public String getattendId() {
            return attendId;
        }

        public String getattendDate() {
            return attendDate;
        }

        public String getAttendDescription() {
            return attendDescription;
        }
    }
}
