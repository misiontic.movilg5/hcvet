package com.ivandelgadog.hcvet.mvp;

public interface ConsultMVP {

    interface Model{
        boolean searchDataConsult(String consultId);
    }

    //Cuando hago alguna acción en la pantalla (Presionar botones)
    interface Presenter{
        void searchConsult();
        void cleanDataConsult();
    }

    interface View{

        ConsultMVP.ConsultInfo getConsultInfo();

        void showConsultIdError(String error);
        void showGeneralError(String error);

        //Pendiente para reutilizar como el de LoginActivity
        void clearData(); //Cuando se pulsa el botón y no han digitado nada y a la vez borra

    }

    class ConsultInfo{
        private String consultId;

        public ConsultInfo(String consultId){
            this.consultId = consultId;
        }

        public String getconsultId() {
            return consultId;
        }

    }

}
