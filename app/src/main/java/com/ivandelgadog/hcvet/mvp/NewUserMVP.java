package com.ivandelgadog.hcvet.mvp;

public interface NewUserMVP {

    interface Model{
        boolean registerDataNewUser(String veterinaryDate, String veterinaryName,
                                    String veterinaryId, String veterinaryNames,
                                    String veterinaryEmail, String veterinaryAddress,
                                    String veterinaryPassword, String veterinaryPasswordAgain);
    }

    //Cuando hago alguna acción en la pantalla (Presionar botones)
    interface Presenter{
        void registerNewUser();
        void cleanDataNewUser();
        void returnNewUser();
    }

    interface View{

        NewUserInfo getNewUserInfo();

        void showVeterinaryNameError(String error);
        void showVeterinaryIdError(String error);
        void showVeterinaryNamesError(String error);
        void showVeterinaryEmailError(String error);
        void showVeterinaryAddressError(String error);
        void showVeterinaryPasswordError(String error);
        void showVeterinaryPasswordAgainError(String error);
        void showGeneralError(String error);

        //Pendiente para reutilizar como el de LoginActivity
        void clearData(); //Cuando se pulsa el botón y no han digitado nada y a la vez borra

        void openLoginActivity(); //Una vez se ingresan los datos, vuelve a la pantalla de Login

    }

    class NewUserInfo{
        private String veterinaryDate;
        private String veterinaryName;
        private String veterinaryId;
        private String veterinaryNames;
        private String veterinaryEmail;
        private String veterinaryAddress;
        private String veterinaryPassword;
        private String veterinaryPasswordAgain;

        public NewUserInfo(String veterinaryDate, String veterinaryName, String veterinaryId,
                           String veterinaryNames, String veterinaryEmail, String veterinaryAddress,
                           String veterinaryPassword, String veterinaryPasswordAgain){
            this.veterinaryDate = veterinaryDate;
            this.veterinaryName = veterinaryName;
            this.veterinaryId = veterinaryId;
            this.veterinaryNames = veterinaryNames;
            this.veterinaryEmail = veterinaryEmail;
            this.veterinaryAddress = veterinaryAddress;
            this.veterinaryPassword = veterinaryPassword;
            this.veterinaryPasswordAgain = veterinaryPasswordAgain;
        }

        public String getVeterinaryDate() {
            return veterinaryDate;
        }

        public String getVeterinaryName() {
            return veterinaryName;
        }

        public String getVeterinaryId() {
            return veterinaryId;
        }

        public String getVeterinaryNames() {
            return veterinaryNames;
        }

        public String getVeterinaryEmail() {
            return veterinaryEmail;
        }

        public String getVeterinaryAddress() {
            return veterinaryAddress;
        }

        public String getVeterinaryPassword() {
            return veterinaryPassword;
        }

        public String getVeterinaryPasswordAgain() {
            return veterinaryPasswordAgain;
        }
    }
}
