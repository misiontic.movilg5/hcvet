package com.ivandelgadog.hcvet.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ivandelgadog.hcvet.R;
import com.ivandelgadog.hcvet.mvp.AttendMVP;
import com.ivandelgadog.hcvet.presenter.AttendPresenter;

public class AttendActivity extends AppCompatActivity implements AttendMVP.View {

    private TextInputLayout tilAttendDate;
    private TextInputEditText etAttendDate;
    private TextInputLayout tilAttendId;
    private TextInputEditText etAttendId;
    private AppCompatButton btnAttendSearch;
    private TextInputLayout tilAttendNamePac;
    private TextInputEditText etAttendNamePac;
    private TextInputLayout tilAttendSpeciePac;
    private TextInputEditText etAttendSpeciePac;
    private TextInputLayout tilAttendRacePac;
    private TextInputEditText etAttendRacePac;
    private TextInputLayout tilAttendDescription;
    private TextInputEditText etAttendDescription;
    private AppCompatButton btnRegisterAttend;
    private AppCompatButton btnRegisterClean;

    private DrawerLayout drawerLayout;
    private MaterialToolbar appBar;
    private NavigationView navigationDrawer;

    private AttendMVP.Presenter presenter;

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attend);

        presenter = new AttendPresenter(this);
        initUI();
    }

    private void initUI(){
        tilAttendDate = findViewById(R.id.til_attend_date);
        etAttendDate = findViewById(R.id.et_attend_date);
        tilAttendId = findViewById(R.id.til_attend_id);
        etAttendId = findViewById(R.id.et_attend_id);
        tilAttendNamePac = findViewById(R.id.til_attend_name_pac);
        etAttendNamePac = findViewById(R.id.et_attend_name_pac);
        tilAttendSpeciePac = findViewById(R.id.til_attend_specie_pac);
        etAttendSpeciePac = findViewById(R.id.et_attend_specie_pac);
        tilAttendRacePac = findViewById(R.id.til_attend_race_pac);
        etAttendRacePac = findViewById(R.id.et_attend_race_pac);
        tilAttendDescription = findViewById(R.id.til_attend_description);
        etAttendDescription = findViewById(R.id.et_attend_description);

        btnAttendSearch = findViewById(R.id.btn_attend_search);
        btnAttendSearch.setOnClickListener(evt-> onRegisterAttendSearchClick());

        btnRegisterAttend = findViewById(R.id.btn_register_attend);
        btnRegisterAttend.setOnClickListener(evt-> presenter.registerAttend());

        btnRegisterClean = findViewById(R.id.btn_register_clean);
        btnRegisterClean.setOnClickListener(evt-> clearData());

        drawerLayout = findViewById(R.id.drawer_layout);

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationItemSelected);

        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        int day = today.monthDay;
        int month = today.month;
        int year = today.year;
        month++;
        etAttendDate.setText(day + "/" + month + "/" + year);
    }

    private void openDrawer(){
        drawerLayout.openDrawer(navigationDrawer);
    }

    private boolean navigationItemSelected(MenuItem menuItem){
        menuItem.setCheckable(true);
        Toast.makeText(this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        drawerLayout.closeDrawer(navigationDrawer);

        if(menuItem.getTitle().equals("Nueva Historia Clínica")){
            Intent intent = new Intent(this, NewHcActivity.class);
            startActivity(intent);
        }
        if(menuItem.getTitle().equals("Atender Paciente")){
            Intent intent = new Intent(this, AttendActivity.class);
            startActivity(intent);
        }
        if(menuItem.getTitle().equals("Consultar Historia Clínica")){
            Intent intent = new Intent(this, ConsultActivity.class);
            startActivity(intent);
        }
        if(menuItem.getTitle().equals("Salir")){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        return true;
    }

    private void onRegisterAttendSearchClick(){

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("hc").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    String namePacient = dataSnapshot.child(etAttendId.getText().toString()).child("patientNameHc").getValue().toString();
                    String speciePacient = dataSnapshot.child(etAttendId.getText().toString()).child("patientSpecieHc").getValue().toString();
                    String racePacient = dataSnapshot.child(etAttendId.getText().toString()).child("patientRaceHc").getValue().toString();

                    etAttendNamePac.setText(namePacient);
                    etAttendSpeciePac.setText(speciePacient);
                    etAttendRacePac.setText(racePacient);
                    etAttendDescription.setEnabled(true);
                    btnRegisterAttend.setEnabled(true);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public AttendMVP.AttendInfo getAttendInfo() {
        return new AttendMVP.AttendInfo(etAttendId.getText().toString(), etAttendDate.getText().toString(), etAttendDescription.getText().toString());
    }

    @Override
    public void showAttendIdError(String error) {
        tilAttendId.setError(error);
    }

    @Override
    public void showAttendDescriptionError(String error) {
        tilAttendDescription.setError(error);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(AttendActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearData() {
        tilAttendId.setError("");
        etAttendId.setText("");
        tilAttendDescription.setError("");
        etAttendDescription.setText("");
        etAttendNamePac.setText("");
        etAttendSpeciePac.setText("");
        etAttendRacePac.setText("");
        etAttendDescription.setEnabled(false);
        btnRegisterAttend.setEnabled(false);
    }
}