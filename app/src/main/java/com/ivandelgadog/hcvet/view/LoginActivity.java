package com.ivandelgadog.hcvet.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ivandelgadog.hcvet.R;
import com.ivandelgadog.hcvet.mvp.LoginMVP;
import com.ivandelgadog.hcvet.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;

    private AppCompatButton btnLogin;
    private AppCompatButton btnRegister;
    private AppCompatButton btnMaps;

    private LoginMVP.Presenter presenter; //Gestiona los eventos que va a hacer en la ventana

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter(this);

        initUI();
    }

    private void initUI() {
        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener((evt)-> presenter.loginWithEmail());

        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener((evt)-> presenter.registerNewUser());
        
        btnMaps = findViewById(R.id.btn_maps);
        btnMaps.setOnClickListener((evt)-> onClicViewMaps());

    }

    private void onClicViewMaps() {

        Bundle params = new Bundle();
        params.putString("address", "Carrera 12 # 10 - 32, Tunja, Boyacá");
        params.putString("address1", "Calle 47 # 54 - 54, Tunja, Boyacá");
        params.putString("address2", "Carrera 11 # 11 - 44, Tunja, Boyacá");

        Intent intent = new Intent(this, LocationActivity.class);
        intent.putExtras(params);
        startActivity(intent);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(etEmail.getText().toString(), etPassword.getText().toString());
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearData() {
        tilEmail.setError("");
        etEmail.setText("");
        tilPassword.setError("");
        etPassword.setText("");
    }

    @Override
    public void openNewHcActivity() {
        Intent intent = new Intent(this, NewHcActivity.class);
        startActivity(intent);
    }

    @Override
    public void openNewUserActivity() {
        Intent intent = new Intent(this, NewUserActivity.class);
        startActivity(intent);
    }
}