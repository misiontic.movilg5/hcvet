package com.ivandelgadog.hcvet.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.ivandelgadog.hcvet.R;
import com.ivandelgadog.hcvet.mvp.NewHcMVP;
import com.ivandelgadog.hcvet.presenter.NewHcPresenter;

public class NewHcActivity extends AppCompatActivity implements NewHcMVP.View {

    private TextInputLayout tilHcDate;
    private TextInputEditText etHcDate;
    private TextInputLayout tilPatienteId;
    private TextInputEditText etPatientId;
    private TextInputLayout tilPatienteName;
    private TextInputEditText etPatienteName;
    private TextInputLayout tilSpeciesName;
    private TextInputEditText etSpeciesName;
    private TextInputLayout tilAgePatient;
    private TextInputEditText etAgePatient;
    private TextInputLayout tilRacePatient;
    private TextInputEditText etRacePatient;
    /*spiner*/

    private TextInputLayout tilOwnerName;
    private TextInputEditText etOwnerName;
    private TextInputLayout tilOwnerEmail;
    private TextInputEditText etOwnerEmail;

    private AppCompatButton btnRegisterNewHc;
    private AppCompatButton btnRegisterClean;

    private DrawerLayout drawerLayout;
    private MaterialToolbar appBar;
    private NavigationView navigationDrawer;

    private NewHcMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_hc);

        presenter = new NewHcPresenter(this);
        initUI();
    }

    private void initUI(){
        tilHcDate = findViewById(R.id.til_hc_date);
        etHcDate = findViewById(R.id.et_hc_date);

        tilPatienteId = findViewById(R.id.til_patient_id);
        etPatientId = findViewById(R.id.et_patient_id);

        tilPatienteName = findViewById(R.id.til_patient_name);
        etPatienteName = findViewById(R.id.et_patient_name);

        tilSpeciesName = findViewById(R.id.til_species_name);
        etSpeciesName = findViewById(R.id.et_species_name);

        tilAgePatient= findViewById(R.id.til_age_patient);
        etAgePatient= findViewById(R.id.et_age_patient);
        tilRacePatient= findViewById(R.id.til_race_patient);
        etRacePatient= findViewById(R.id.et_race_patient);

        tilOwnerName= findViewById(R.id.til_owner_name);
        etOwnerName= findViewById(R.id.et_owner_name);
        tilOwnerEmail= findViewById(R.id.til_owner_email);
        etOwnerEmail= findViewById(R.id.et_owner_email);

        btnRegisterNewHc = findViewById(R.id.btn_register_new_hc);
        btnRegisterNewHc.setOnClickListener((evt)-> presenter.registerNewHc());

        btnRegisterClean = findViewById(R.id.btn_register_clean);
        btnRegisterClean.setOnClickListener((evt)-> clearData());

        drawerLayout = findViewById(R.id.drawer_layout);

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationItemSelected);

        //Inicializando fecha
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        int day = today.monthDay;
        int month = today.month;
        int year = today.year;
        month++;
        etHcDate.setText(day + "/" + month + "/" + year);
    }

    private void openDrawer(){
        drawerLayout.openDrawer(navigationDrawer);
    }

    private boolean navigationItemSelected(MenuItem menuItem){
        menuItem.setCheckable(true);
        Toast.makeText(this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        drawerLayout.closeDrawer(navigationDrawer);

        if(menuItem.getTitle().equals("Nueva Historia Clínica")){
            Intent intent = new Intent(this, NewHcActivity.class);
            startActivity(intent);
        }
        if(menuItem.getTitle().equals("Atender Paciente")){
            Intent intent = new Intent(this, AttendActivity.class);
            startActivity(intent);
        }
        if(menuItem.getTitle().equals("Consultar Historia Clínica")){
            Intent intent = new Intent(this, ConsultActivity.class);
            startActivity(intent);
        }
        if(menuItem.getTitle().equals("Salir")){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public NewHcMVP.NewHcInfo getNewHcInfo() {
        return new NewHcMVP.NewHcInfo(etHcDate.getText().toString(), etPatientId.getText().toString(),
                etPatienteName.getText().toString(), etSpeciesName.getText().toString(), etAgePatient.getText().toString(),
                etRacePatient.getText().toString(), etOwnerName.getText().toString(), etOwnerEmail.getText().toString());
    }

    @Override
    public void showpatientIdHcError(String error) {
        tilPatienteId.setError(error);
    }

    @Override
    public void showpatientNameHcError(String error) {
        tilPatienteName.setError(error);
    }

    @Override
    public void showpatientSpecieError(String error) {
        tilSpeciesName.setError(error);
    }

    @Override
    public void showpatientAgeError(String error) {
        tilAgePatient.setError(error);
    }

    @Override
    public void showpatientRaceHcError(String error) {
        tilRacePatient.setError(error);
    }

    @Override
    public void showownerNameHcError(String error) {
        tilOwnerName.setError(error);
    }

    @Override
    public void showownerEmailHcError(String error) {
        tilOwnerEmail.setError(error);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(NewHcActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearData() {
        tilPatienteId.setError("");
        etPatientId.setText("");
        tilPatienteName.setError("");
        etPatienteName.setText("");
        tilSpeciesName.setError("");
        etSpeciesName.setText("");
        tilAgePatient.setError("");
        etAgePatient.setText("");
        tilRacePatient.setError("");
        etRacePatient.setText("");
        tilOwnerName.setError("");
        etOwnerName.setText("");
        tilOwnerEmail.setError("");
        etOwnerEmail.setText("");
    }
}