package com.ivandelgadog.hcvet.view;

import androidx.appcompat.app.AppCompatActivity;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.ivandelgadog.hcvet.R;
import java.util.List;
import java.util.Locale;

public class LocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        String address = getIntent().getStringExtra("address");
        String address1 = getIntent().getStringExtra("address1");
        String address2 = getIntent().getStringExtra("address2");

        LatLng location = new LatLng(4, -72);
        try {
            Geocoder geo = new Geocoder(LocationActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocationName(address, 1);
            if (!addresses.isEmpty()) {
                location = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            }
        }catch(Exception e){
                e.printStackTrace();
        }

        LatLng location1 = new LatLng(4, -72);
        try {
            Geocoder geo = new Geocoder(LocationActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocationName(address1, 1);
            if (!addresses.isEmpty()) {
                location1 = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        LatLng location2 = new LatLng(4, -72);
        try {
            Geocoder geo = new Geocoder(LocationActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocationName(address2, 1);
            if (!addresses.isEmpty()) {
                location2 = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        // Add a marker in Sydney and move the camera
        mMap.addMarker(new MarkerOptions()
                .position(location)
                .title(address + " - " + address));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 14f));

        // Add a marker in Sydney and move the camera
        mMap.addMarker(new MarkerOptions()
                .position(location1)
                .title(address1 + " - " + address1));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location1, 14f));

        // Add a marker in Sydney and move the camera
        mMap.addMarker(new MarkerOptions()
                .position(location2)
                .title(address2 + " - " + address2));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location2, 14f));
    }
}