package com.ivandelgadog.hcvet.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.ivandelgadog.hcvet.R;
import com.ivandelgadog.hcvet.mvp.NewUserMVP;
import com.ivandelgadog.hcvet.presenter.NewUserPresenter;

public class NewUserActivity extends AppCompatActivity implements NewUserMVP.View {

    private TextInputLayout tilVeterinaryDate;
    private TextInputEditText etVeterinaryDate;
    private TextInputLayout tilVeterinaryName;
    private TextInputEditText etVeterinaryName;
    private TextInputLayout tilVeterinaryId;
    private TextInputEditText etVeterinaryId;
    private TextInputLayout tilVeterinaryNames;
    private TextInputEditText etVeterinaryNames;
    private TextInputLayout tilVeterinaryEmail;
    private TextInputEditText etVeterinaryEmail;
    private TextInputLayout tilVeterinaryAddress;
    private TextInputEditText etVeterinaryAddress;
    private TextInputLayout tilVeterinaryPassword;
    private TextInputEditText etVeterinaryPassword;
    private TextInputLayout tilVeterinaryPassword2;
    private TextInputEditText etVeterinaryPassword2;
    private AppCompatButton btnRegisterNewUser;
    private AppCompatButton btnRegisterClean;
    private AppCompatButton btnReturn;

    private NewUserMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);

        presenter = new NewUserPresenter(this);
        initUI();
    }

    private void initUI(){
        tilVeterinaryDate = findViewById(R.id.til_veterinary_date);
        etVeterinaryDate = findViewById(R.id.et_veterinary_date);

        tilVeterinaryName = findViewById(R.id.til_veterinary_name);
        etVeterinaryName = findViewById(R.id.et_veterinary_name);

        tilVeterinaryId = findViewById(R.id.til_veterinary_id);
        etVeterinaryId = findViewById(R.id.et_veterinary_id);

        tilVeterinaryNames = findViewById(R.id.til_veterinary_names);
        etVeterinaryNames = findViewById(R.id.et_veterinary_names);

        tilVeterinaryEmail = findViewById(R.id.til_veterinary_email);
        etVeterinaryEmail = findViewById(R.id.et_veterinary_email);

        tilVeterinaryAddress = findViewById(R.id.til_veterinary_address);
        etVeterinaryAddress = findViewById(R.id.et_veterinary_address);

        tilVeterinaryPassword = findViewById(R.id.til_veterinary_password);
        etVeterinaryPassword = findViewById(R.id.et_veterinary_password);

        tilVeterinaryPassword2 = findViewById(R.id.til_veterinary_password2);
        etVeterinaryPassword2 = findViewById(R.id.et_veterinary_password2);

        btnRegisterNewUser = findViewById(R.id.btn_register_new_user);
        btnRegisterNewUser.setOnClickListener(evt -> presenter.registerNewUser());

        btnRegisterClean = findViewById(R.id.btn_register_clean);
        btnRegisterClean.setOnClickListener(evt -> presenter.cleanDataNewUser());

        btnReturn = findViewById(R.id.btn_return);
        btnReturn.setOnClickListener(evt -> presenter.returnNewUser());

        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        int day = today.monthDay;
        int month = today.month;
        int year = today.year;
        month++;
        etVeterinaryDate.setText(day + "/" + month + "/" + year);
    }

    @Override
    public NewUserMVP.NewUserInfo getNewUserInfo() {
        return new NewUserMVP.NewUserInfo(etVeterinaryDate.getText().toString(), etVeterinaryName.getText().toString(),
                etVeterinaryId.getText().toString(), etVeterinaryNames.getText().toString(), etVeterinaryEmail.getText().toString(),
                etVeterinaryAddress.getText().toString(), etVeterinaryPassword.getText().toString(), etVeterinaryPassword2.getText().toString());
    }

    @Override
    public void showVeterinaryNameError(String error) {
        tilVeterinaryName.setError(error);
    }

    @Override
    public void showVeterinaryIdError(String error) {
        tilVeterinaryId.setError(error);
    }

    @Override
    public void showVeterinaryNamesError(String error) {
        tilVeterinaryNames.setError(error);
    }

    @Override
    public void showVeterinaryEmailError(String error) {
        tilVeterinaryEmail.setError(error);
    }

    @Override
    public void showVeterinaryAddressError(String error) {
        tilVeterinaryAddress.setError(error);
    }

    @Override
    public void showVeterinaryPasswordError(String error) {
        tilVeterinaryPassword.setError(error);
    }

    @Override
    public void showVeterinaryPasswordAgainError(String error) {
        tilVeterinaryPassword2.setError(error);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(NewUserActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearData() {
        tilVeterinaryName.setError("");
        etVeterinaryName.setText("");
        tilVeterinaryId.setError("");
        etVeterinaryId.setText("");
        tilVeterinaryNames.setError("");
        etVeterinaryNames.setText("");
        tilVeterinaryEmail.setError("");
        etVeterinaryEmail.setText("");
        tilVeterinaryAddress.setError("");
        etVeterinaryAddress.setText("");
        tilVeterinaryPassword.setError("");
        etVeterinaryPassword.setText("");
        tilVeterinaryPassword2.setError("");
        etVeterinaryPassword2.setText("");
    }

    @Override
    public void openLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}