package com.ivandelgadog.hcvet.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ivandelgadog.hcvet.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}