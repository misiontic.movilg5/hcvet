package com.ivandelgadog.hcvet.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ivandelgadog.hcvet.R;
import com.ivandelgadog.hcvet.mvp.ConsultMVP;
import com.ivandelgadog.hcvet.presenter.ConsultPresenter;

public class ConsultActivity extends AppCompatActivity implements ConsultMVP.View {

    TextInputLayout tilConsultId;
    TextInputEditText etConsultId;
    AppCompatButton btnConsultSearch;
    TextInputLayout tilConsultNamePac;
    TextInputEditText etConsultNamePac;
    TextInputLayout tilConsultSpeciePac;
    TextInputEditText etConsultSpeciePac;
    TextInputLayout tilConsultRacePac;
    TextInputEditText etConsultRacePac;
    TextInputLayout tilConsultHc;
    TextInputEditText etConsultHc;
    AppCompatButton btnRegisterClean;

    private DrawerLayout drawerLayout;
    private MaterialToolbar appBar;
    private NavigationView navigationDrawer;

    private ConsultMVP.Presenter presenter;

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_hc);

        presenter = new ConsultPresenter(this);
        initUI();
    }

    private void initUI(){
        tilConsultId = findViewById(R.id.til_consult_id);
        etConsultId = findViewById(R.id.et_consult_id);

        btnConsultSearch = findViewById(R.id.btn_consult_search);
        btnConsultSearch.setOnClickListener((evt)-> onConsultSearchClick());

        tilConsultNamePac = findViewById(R.id.til_consult_name_pac);
        etConsultNamePac = findViewById(R.id.et_consult_name_pac);
        tilConsultSpeciePac = findViewById(R.id.til_consult_specie_pac);
        etConsultSpeciePac = findViewById(R.id.et_consult_specie_pac);
        tilConsultRacePac = findViewById(R.id.til_consult_race_pac);
        etConsultRacePac = findViewById(R.id.et_consult_race_pac);
        tilConsultHc = findViewById(R.id.til_consult_hc);
        etConsultHc = findViewById(R.id.et_consult_hc);

        btnRegisterClean = findViewById(R.id.btn_register_clean);
        btnRegisterClean.setOnClickListener((evt)-> presenter.cleanDataConsult());

        drawerLayout = findViewById(R.id.drawer_layout);

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationItemSelected);
    }

    private void openDrawer(){
        drawerLayout.openDrawer(navigationDrawer);
    }

    private boolean navigationItemSelected(MenuItem menuItem){
        menuItem.setCheckable(true);
        Toast.makeText(this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        drawerLayout.closeDrawer(navigationDrawer);

        if(menuItem.getTitle().equals("Nueva Historia Clínica")){
            Intent intent = new Intent(this, NewHcActivity.class);
            startActivity(intent);
        }
        if(menuItem.getTitle().equals("Atender Paciente")){
            Intent intent = new Intent(this, AttendActivity.class);
            startActivity(intent);
        }
        if(menuItem.getTitle().equals("Consultar Historia Clínica")){
            Intent intent = new Intent(this, ConsultActivity.class);
            startActivity(intent);
        }
        if(menuItem.getTitle().equals("Salir")){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        return true;
    }

    private void onConsultSearchClick(){

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("hc").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    String namePacient = dataSnapshot.child(etConsultId.getText().toString()).child("patientNameHc").getValue().toString();
                    String speciePacient = dataSnapshot.child(etConsultId.getText().toString()).child("patientSpecieHc").getValue().toString();
                    String racePacient = dataSnapshot.child(etConsultId.getText().toString()).child("patientRaceHc").getValue().toString();
                    String totalHc = dataSnapshot.child(etConsultId.getText().toString()).child("veterinaryConsult").getValue().toString();
                    totalHc = totalHc.substring(1,totalHc.length()-1);

                    etConsultNamePac.setText(namePacient);
                    etConsultSpeciePac.setText(speciePacient);
                    etConsultRacePac.setText(racePacient);
                    etConsultHc.setText(totalHc);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public ConsultMVP.ConsultInfo getConsultInfo() {
        return new ConsultMVP.ConsultInfo(etConsultId.getText().toString());
    }

    @Override
    public void showConsultIdError(String error) {
        tilConsultId.setError(error);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(ConsultActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearData() {
        tilConsultId.setError("");
        etConsultId.setText("");
        etConsultNamePac.setText("");
        etConsultSpeciePac.setText("");
        etConsultRacePac.setText("");
        etConsultHc.setText("");
    }
}