package com.ivandelgadog.hcvet.presenter;

import com.ivandelgadog.hcvet.model.ConsultInteractor;
import com.ivandelgadog.hcvet.mvp.ConsultMVP;

public class ConsultPresenter implements ConsultMVP.Presenter{

    private ConsultMVP.View view;
    private ConsultMVP.Model model;

    public ConsultPresenter(ConsultMVP.View view){
        this.view = view;
        this.model = new ConsultInteractor();
    }

    @Override
    public void searchConsult() {

        boolean error1 = false;

        view.showConsultIdError("");

        ConsultMVP.ConsultInfo ConsultInfo = view.getConsultInfo();
        if(ConsultInfo.getconsultId().trim().isEmpty()){
            view.showConsultIdError("La identificación del paciente es obligatoria");
            error1 = true;
        }

        if(!error1){
            if(model.searchDataConsult(ConsultInfo.getconsultId().trim())){
                view.showGeneralError("Consulta exitosa");
            }
            else{
                view.showGeneralError("No se logró realizar la consulta");
            }
            view.showGeneralError("Verifique los datos");
        }

    }

    @Override
    public void cleanDataConsult() {
        view.clearData();
    }
}
