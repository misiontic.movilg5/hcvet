package com.ivandelgadog.hcvet.presenter;

import com.ivandelgadog.hcvet.model.AttendInteractor;
import com.ivandelgadog.hcvet.mvp.AttendMVP;

public class AttendPresenter implements AttendMVP.Presenter{

    private AttendMVP.View view;
    private AttendMVP.Model model;

    public AttendPresenter(AttendMVP.View view){
        this.view = view;
        this.model = new AttendInteractor();
    }

    @Override
    public void searchAttend() {

        boolean error1 = false;

        view.showAttendIdError("");

        AttendMVP.AttendInfo AttendInfo = view.getAttendInfo();
        if(AttendInfo.getattendId().trim().isEmpty()){
            view.showAttendIdError("La identificación del paciente es obligatoria");
            error1 = true;
        }

        if(!error1){
            if(model.searchDataAttend(AttendInfo.getattendId().trim())){
                view.showGeneralError("Consulta de paciente exitosa");
            }
            else{
                view.showGeneralError("No se logró consultar paciente");
            }
            view.showGeneralError("Verifique los datos");
        }
    }

    @Override
    public void registerAttend() {

        boolean error2 = false;

        view.showAttendIdError("");
        view.showAttendDescriptionError("");

        AttendMVP.AttendInfo AttendInfo = view.getAttendInfo();
        if(AttendInfo.getattendId().trim().isEmpty()){
            view.showAttendIdError("La identificación del paciente es obligatoria");
            error2 = true;
        }
        if(AttendInfo.getAttendDescription().trim().isEmpty()){
            view.showAttendDescriptionError("La descripción de la consulta es obligatoria");
            error2 = true;
        }

        if(!error2){
            if(model.registerDataAttend(AttendInfo.getattendId().trim(), AttendInfo.getattendDate().trim(),
                    AttendInfo.getAttendDescription().trim())) {

                view.showGeneralError("Registro de consulta exitosa");
                //cleanDataAttend();
            }
            else{
                view.showGeneralError("No se logró registrar la consulta");
            }
            view.showGeneralError("Verifique los datos");
        }
    }

    @Override
    public void cleanDataAttend() {
        view.clearData();
    }
}
