package com.ivandelgadog.hcvet.presenter;

import android.os.Bundle;

import com.ivandelgadog.hcvet.model.LoginInteractor;
import com.ivandelgadog.hcvet.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {

    private LoginMVP.View view;
    private LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view){
        this.view = view;
        this.model = new LoginInteractor(view.getActivity());
    }

    @Override
    public void loginWithEmail() {
        boolean error = false;

        view.showEmailError("");
        view.showPasswordError("");

        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();

        //Valida los campos
        if(loginInfo.getEmail().trim().isEmpty()){
            view.showEmailError("Correo electrónico es obligatorio");
            error = true;
        }else if(!isEmailValid(loginInfo.getEmail().trim())){
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }

        if(loginInfo.getPassword().trim().isEmpty()){
            view.showPasswordError("Contraseña es obligatoria");
            error = true;
        }else if(!isPasswordValid(loginInfo.getPassword().trim())){
            view.showPasswordError("Contraseña no cumple criterios de seguridad");
            error = true;
        }

        //TODO validar que usuario y contraseña sean las correctas

        if(!error){
            new Thread(() -> {
                model.validateCredentials(loginInfo.getEmail().trim(),
                        loginInfo.getPassword().trim(),
                        new LoginMVP.Model.ValidateCredentialsCallback() {
                            @Override
                            public void onSuccess() {
                                view.getActivity().runOnUiThread(() -> {
                                    view.openNewHcActivity();
                                });
                            }

                            @Override
                            public void onFailure(String error) {
                                view.getActivity().runOnUiThread(() -> {
                                    view.showGeneralError(error);
                                });
                            }
                        });
            }).start();

        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    private boolean isEmailValid(String email) {
        return (email.contains("@")) && (email.endsWith(".com") || (email.endsWith(".co")));
    }

    @Override
    public void registerNewUser() {
        view.openNewUserActivity();
    }
}
