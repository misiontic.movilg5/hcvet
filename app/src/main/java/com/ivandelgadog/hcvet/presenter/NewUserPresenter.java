package com.ivandelgadog.hcvet.presenter;

import com.ivandelgadog.hcvet.model.NewUserInteractor;
import com.ivandelgadog.hcvet.mvp.NewUserMVP;

public class NewUserPresenter implements NewUserMVP.Presenter {

    private NewUserMVP.View view;
    private NewUserMVP.Model model;

    public NewUserPresenter(NewUserMVP.View view){
        this.view = view;
        this.model = new NewUserInteractor();
    }

    @Override
    public void registerNewUser() {

        boolean error = false;

        view.showVeterinaryNameError("");
        view.showVeterinaryIdError("");
        view.showVeterinaryNamesError("");
        view.showVeterinaryEmailError("");
        view.showVeterinaryAddressError("");
        view.showVeterinaryPasswordError("");
        view.showVeterinaryPasswordAgainError("");

        NewUserMVP.NewUserInfo newUserInfo = view.getNewUserInfo();
        if(newUserInfo.getVeterinaryName().trim().isEmpty()){
            view.showVeterinaryNameError("El nombre de la veterinaria es obligatorio");
            error = true;
        }
        if(newUserInfo.getVeterinaryId().trim().isEmpty()){
            view.showVeterinaryIdError("El número de identificación del veterinario(a) es obligatorio");
            error = true;
        }
        if(newUserInfo.getVeterinaryNames().trim().isEmpty()){
            view.showVeterinaryNamesError("Los nombres y apellidos del veterinario(a) son obligatorios");
            error = true;
        }
        if(newUserInfo.getVeterinaryEmail().trim().isEmpty()){
            view.showVeterinaryEmailError("El correo electrónico del veterinario(a) es obligatorio");
            error = true;
        }else if(!isEmailValid(newUserInfo.getVeterinaryEmail().trim())){
            view.showVeterinaryEmailError("El correo electrónico no es válido");
            error = true;
        }
        if(newUserInfo.getVeterinaryAddress().trim().isEmpty()){
            view.showVeterinaryAddressError("La dirección del veterinario(a) es obligatoria");
            error = true;
        }
        if(newUserInfo.getVeterinaryPassword().trim().isEmpty()){
            view.showVeterinaryPasswordError("La contraseña del veterinario(a) es obligatoria");
            error = true;
        }else if(!isPasswordValid(newUserInfo.getVeterinaryPassword().trim())){
            view.showVeterinaryPasswordError("La contraseña no cumple criterios de seguridad");
            error = true;
        }
        if(newUserInfo.getVeterinaryPasswordAgain().trim().isEmpty()){
            view.showVeterinaryPasswordAgainError("La confirmación de la contraseña del veterinario(a) es obligatoria");
            error = true;
        }else if(!isPasswordValid(newUserInfo.getVeterinaryPasswordAgain().trim())){
            view.showVeterinaryPasswordAgainError("La confirmación de la contraseña no cumple criterios de seguridad");
            error = true;
        }
        if(!isEmailValidEmail2(newUserInfo.getVeterinaryPassword().trim(),newUserInfo.getVeterinaryPasswordAgain().trim())){
            view.showVeterinaryPasswordAgainError("Las contraseñas no coinciden");
            error = true;
        }

        if(!error){
            if(model.registerDataNewUser(newUserInfo.getVeterinaryDate().trim(), newUserInfo.getVeterinaryName().trim(),
                    newUserInfo.getVeterinaryId().trim(), newUserInfo.getVeterinaryNames().trim(),
                    newUserInfo.getVeterinaryEmail().trim(), newUserInfo.getVeterinaryAddress().trim(),
                    newUserInfo.getVeterinaryPassword().trim(), newUserInfo.getVeterinaryPasswordAgain().trim())) {

                view.openLoginActivity();
                view.showGeneralError("Registro de veterinario(a) exitoso");
            }
            else{
                view.showGeneralError("No se logró registrar el veterinario");
            }
            view.showGeneralError("Verifique los datos");
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    private boolean isEmailValid(String email) {
        return (email.contains("@")) && (email.endsWith(".com") || (email.endsWith(".co")));
    }

    private boolean isEmailValidEmail2(String email1, String email2) {
        return (email1.equals(email2));
    }

    @Override
    public void cleanDataNewUser() {
        view.clearData();
    }

    @Override
    public void returnNewUser() {
        view.openLoginActivity();
    }
}
