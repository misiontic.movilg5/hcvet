package com.ivandelgadog.hcvet.presenter;

import com.ivandelgadog.hcvet.model.NewHcInteractor;
import com.ivandelgadog.hcvet.mvp.NewHcMVP;

public class NewHcPresenter implements NewHcMVP.Presenter{

    private NewHcMVP.View view;
    private NewHcMVP.Model model;

    public NewHcPresenter(NewHcMVP.View view){
        this.view = view;
        this.model = new NewHcInteractor();
    }

    @Override
    public void registerNewHc() {

        boolean error = false;

        view.showpatientIdHcError("");
        view.showpatientNameHcError("");
        view.showpatientSpecieError("");
        view.showpatientAgeError("");
        view.showpatientRaceHcError("");
        view.showownerNameHcError("");
        view.showownerEmailHcError("");

        NewHcMVP.NewHcInfo newHcInfo = view.getNewHcInfo();
        if(newHcInfo.getPatientIdHc().trim().isEmpty()){
            view.showpatientIdHcError("El id del paciente es obligatorio");
            error = true;
        }
        if(newHcInfo.getPatientNameHc().trim().isEmpty()){
            view.showpatientNameHcError("El nombre del paciente es obligatorio");
            error = true;
        }
        if(newHcInfo.getPatientSpecieHc().trim().isEmpty()){
            view.showpatientSpecieError("La especie del paciente es obligatoria");
            error = true;
        }
        if(newHcInfo.getPatientAgeHc().trim().isEmpty()){
            view.showpatientAgeError("La edad del paciente es obligatoria");
            error = true;
        }
        if(newHcInfo.getPatientRaceHc().trim().isEmpty()){
            view.showpatientRaceHcError("La raza del paciente es obligatoria");
            error = true;
        }
        if(newHcInfo.getOwnerNameHc().trim().isEmpty()){
            view.showownerNameHcError("El nombre del propietario es obligatorio");
            error = true;
        }
        if(newHcInfo.getOwnerEmailHc().trim().isEmpty()){
            view.showownerEmailHcError("El correo electrónico del propietario es obligatorio");
            error = true;
        }else if(!isEmailValid(newHcInfo.getOwnerEmailHc().trim())){
            view.showownerEmailHcError("El correo electrónico no es válido");
            error = true;
        }

        if(!error){
            if(model.registerDataNewHc(newHcInfo.getDateHc().trim(), newHcInfo.getPatientIdHc().trim(),
                    newHcInfo.getPatientNameHc().trim(), newHcInfo.getPatientSpecieHc().trim(),
                    newHcInfo.getPatientAgeHc().trim(), newHcInfo.getPatientRaceHc().trim(),
                    newHcInfo.getOwnerNameHc().trim(), newHcInfo.getOwnerEmailHc().trim())) {

                view.showGeneralError("Registro de historia clínica exitosa");
            }
            else{
                view.showGeneralError("No se logró registrar la historia clínica");
            }
            view.showGeneralError("Verifique los datos");
        }
    }

    private boolean isEmailValid(String email) {
        return (email.contains("@")) && (email.endsWith(".com") || (email.endsWith(".co")));
    }

    @Override
    public void cleanDataNewHc() {
        view.clearData();
    }
}
