package com.ivandelgadog.hcvet.model;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ivandelgadog.hcvet.mvp.NewHcMVP;

public class NewHcInteractor implements NewHcMVP.Model{

    DatabaseReference databaseReference;

    @Override
    public boolean registerDataNewHc(String dateHc, String patientIdHc, String patientNameHc,
                                     String patientSpecieHc, String patientAgeHc,
                                     String patientRaceHc, String ownerNameHc,
                                     String ownerEmailHc) {

        databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("hc").child(patientIdHc).child("dateHc").setValue(dateHc);
        databaseReference.child("hc").child(patientIdHc).child("patientIdHc").setValue(patientIdHc);
        databaseReference.child("hc").child(patientIdHc).child("patientNameHc").setValue(patientNameHc);
        databaseReference.child("hc").child(patientIdHc).child("patientSpecieHc").setValue(patientSpecieHc);
        databaseReference.child("hc").child(patientIdHc).child("patientAgeHc").setValue(patientAgeHc);
        databaseReference.child("hc").child(patientIdHc).child("patientRaceHc").setValue(patientRaceHc);
        databaseReference.child("hc").child(patientIdHc).child("ownerNameHc").setValue(ownerNameHc);
        databaseReference.child("hc").child(patientIdHc).child("ownerEmailHc").setValue(ownerEmailHc);

        return true;
    }
}
