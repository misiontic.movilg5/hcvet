package com.ivandelgadog.hcvet.model.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.ivandelgadog.hcvet.model.database.dao.UserDao;
import com.ivandelgadog.hcvet.model.database.entities.User;

@Database(entities = {User.class}, version = 1)
public abstract class VeterinaryDatabase extends RoomDatabase {

    public abstract UserDao getUserDao();

    private static volatile VeterinaryDatabase INSTANCE;

    public static VeterinaryDatabase getDatabase(Context context){
        if(INSTANCE == null){
            INSTANCE = Room
                    .databaseBuilder(context.getApplicationContext(), VeterinaryDatabase.class, "database-name")
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }
}
