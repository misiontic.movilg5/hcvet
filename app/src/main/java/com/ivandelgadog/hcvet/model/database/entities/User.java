package com.ivandelgadog.hcvet.model.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class User implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String veterinaryDate;
    private String veterinaryName;
    private String veterinaryId;
    private String veterinaryNames;

    @ColumnInfo(index = true)
    private String veterinaryEmail;
    private String veterinaryTelephone;
    private String veterinaryPassword;
    private String veterinaryPasswordAgain;

    public User(){

    }

    public User(String veterinaryDate, String veterinaryName, String veterinaryId,
                String veterinaryNames, String veterinaryEmail, String veterinaryTelephone,
                String veterinaryPassword, String veterinaryPasswordAgain) {
        this.veterinaryDate = veterinaryDate;
        this.veterinaryName = veterinaryName;
        this.veterinaryId = veterinaryId;
        this.veterinaryNames = veterinaryNames;
        this.veterinaryEmail = veterinaryEmail;
        this.veterinaryTelephone = veterinaryTelephone;
        this.veterinaryPassword = veterinaryPassword;
        this.veterinaryPasswordAgain = veterinaryPasswordAgain;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVeterinaryDate() {
        return veterinaryDate;
    }

    public void setVeterinaryDate(String veterinaryDate) {
        this.veterinaryDate = veterinaryDate;
    }

    public String getVeterinaryName() {
        return veterinaryName;
    }

    public void setVeterinaryName(String veterinaryName) {
        this.veterinaryName = veterinaryName;
    }

    public String getVeterinaryId() {
        return veterinaryId;
    }

    public void setVeterinaryId(String veterinaryId) {
        this.veterinaryId = veterinaryId;
    }

    public String getVeterinaryNames() {
        return veterinaryNames;
    }

    public void setVeterinaryNames(String veterinaryNames) {
        this.veterinaryNames = veterinaryNames;
    }

    public String getVeterinaryEmail() {
        return veterinaryEmail;
    }

    public void setVeterinaryEmail(String veterinaryEmail) {
        this.veterinaryEmail = veterinaryEmail;
    }

    public String getVeterinaryTelephone() {
        return veterinaryTelephone;
    }

    public void setVeterinaryTelephone(String veterinaryTelephone) {
        this.veterinaryTelephone = veterinaryTelephone;
    }

    public String getVeterinaryPassword() {
        return veterinaryPassword;
    }

    public void setVeterinaryPassword(String veterinaryPassword) {
        this.veterinaryPassword = veterinaryPassword;
    }

    public String getVeterinaryPasswordAgain() {
        return veterinaryPasswordAgain;
    }

    public void setVeterinaryPasswordAgain(String veterinaryPasswordAgain) {
        this.veterinaryPasswordAgain = veterinaryPasswordAgain;
    }

    @Override
    public String toString() {
        return "User{" +
                "veterinaryDate='" + veterinaryDate + '\'' +
                ", veterinaryName='" + veterinaryName + '\'' +
                ", veterinaryId='" + veterinaryId + '\'' +
                ", veterinaryNames='" + veterinaryNames + '\'' +
                ", veterinaryEmail='" + veterinaryEmail + '\'' +
                ", veterinaryTelephone='" + veterinaryTelephone + '\'' +
                ", veterinaryPassword='" + veterinaryPassword + '\'' +
                '}';
    }
}
