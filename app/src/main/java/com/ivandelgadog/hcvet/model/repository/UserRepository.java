package com.ivandelgadog.hcvet.model.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ivandelgadog.hcvet.model.database.VeterinaryDatabase;
import com.ivandelgadog.hcvet.model.database.dao.UserDao;
import com.ivandelgadog.hcvet.model.database.entities.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private static UserRepository instance;

    public static UserRepository getInstance(Context context){
        if(instance==null){
            instance = new UserRepository(context);
        }
        return instance;
    }

    private static Boolean USER_DATABASE = Boolean.FALSE;

    private UserDao userDao;

    private DatabaseReference userRef;

    private UserRepository(Context context){
        userDao = VeterinaryDatabase.getDatabase(context).getUserDao();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userRef = database.getReference("user");
        loadInitialDatabase();
    }

    private void loadInitialDatabase() {
        if(USER_DATABASE) {
            userDao.insert(
                    new User("01/01/2021", "Animal Center", "12345678",
                            "Iván Delgado", "c@c.com", "3214526354",
                            "12345678", "12345678")
            );
        }else{

            String username = "c@c.com".replace('@', '_').replace('.','_');

            userRef.child(username).child("veterinaryDate").setValue("01/01/2021");
            userRef.child(username).child("veterinaryName").setValue("Animal Center");
            userRef.child(username).child("veterinaryId").setValue("12345678");
            userRef.child(username).child("veterinaryNames").setValue("Iván Delgado");
            userRef.child(username).child("veterinaryEmail").setValue("e@f.com");
            userRef.child(username).child("veterinaryTelephone").setValue("3214526354");
            userRef.child(username).child("veterinaryPassword").setValue("12345678");
            userRef.child(username).child("veterinaryPasswordAgain").setValue("12345678");

            username = "d@d.com".replace('@', '_').replace('.','_');

            userRef.child(username).setValue(new User("02/02/2021", "Leo el León", "12345678",
                    "Leonardo Bernal", "d@d.com", "3124567833",
                    "87654321", "87654321"));
        }
    }

    public void getUserByEmail(String email, UserCallback<User> callback) {
        if(USER_DATABASE) {
            callback.onSuccess(userDao.getUserByEmail(email));
        }else{
            String username = email.replace('@', '_').replace('.','_');
            userRef.child(username).get()
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            User value = task.getResult().getValue(User.class);
                            callback.onSuccess(value);
                            Log.d(UserRepository.class.getSimpleName(), "Value is: " + value);
                        }else{
                            callback.onFailure();
                        }
                    });
        }
    }

    public void getAll(UserCallback<List<User>> callback){
        userRef.get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                DataSnapshot dataSnapshot = task.getResult();
                if(dataSnapshot.hasChildren()){
                    List<User> users = new ArrayList<>();
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        snapshot.child("veterinaryName").getValue(String.class); //Ojo
                        User user = snapshot.getValue(User.class);
                        Log.d(UserRepository.class.getSimpleName(), user.toString());
                        users.add(user);
                    }
                    callback.onSuccess(users);
                }else{
                    callback.onSuccess(new ArrayList<>());
                }
            }else{
                callback.onFailure();
            }
        });

    }

    private User getUserByEmailDB(String email){
            return userDao.getUserByEmail(email);
    }

    public interface UserCallback<T>{
        void onSuccess(T data);
        void onFailure();
    }
}
