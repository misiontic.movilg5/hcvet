package com.ivandelgadog.hcvet.model;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ivandelgadog.hcvet.mvp.NewUserMVP;

public class NewUserInteractor implements NewUserMVP.Model {

    DatabaseReference databaseReference;

    @Override
    public boolean registerDataNewUser(String veterinaryDate, String veterinaryName,
                                       String veterinaryId, String veterinaryNames,
                                       String veterinaryEmail, String veterinaryAddress,
                                       String veterinaryPassword, String veterinaryPasswordAgain) {

        databaseReference = FirebaseDatabase.getInstance().getReference();

        String username = veterinaryEmail.replace('@', '_').replace('.','_');

        databaseReference.child("user").child(username).child("veterinaryDate").setValue(veterinaryDate);
        databaseReference.child("user").child(username).child("veterinaryName").setValue(veterinaryName);
        databaseReference.child("user").child(username).child("veterinaryId").setValue(veterinaryId);
        databaseReference.child("user").child(username).child("veterinaryNames").setValue(veterinaryName);
        databaseReference.child("user").child(username).child("veterinaryEmail").setValue(veterinaryEmail);
        databaseReference.child("user").child(username).child("veterinaryAddress").setValue(veterinaryAddress);
        databaseReference.child("user").child(username).child("veterinaryPassword").setValue(veterinaryPassword);
        databaseReference.child("user").child(username).child("veterinaryPasswordAgain").setValue(veterinaryPassword);

        return true;
    }
}
