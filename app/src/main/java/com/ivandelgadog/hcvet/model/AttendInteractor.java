package com.ivandelgadog.hcvet.model;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ivandelgadog.hcvet.mvp.AttendMVP;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AttendInteractor implements AttendMVP.Model{

    //private DatabaseReference mDatabase;
    private DatabaseReference databaseReference;

    @Override
    public boolean searchDataAttend(String attendId) {
        /*
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("hc").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    String namePacient = dataSnapshot.child(etAttendId.getText().toString()).child("patientNameHc").getValue().toString();
                    String speciePacient = dataSnapshot.child(etAttendId.getText().toString()).child("patientSpecieHc").getValue().toString();
                    String racePacient = dataSnapshot.child(etAttendId.getText().toString()).child("patientRaceHc").getValue().toString();

                    etAttendNamePac.setText(namePacient);
                    etAttendSpeciePac.setText(speciePacient);
                    etAttendRacePac.setText(racePacient);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        */
        return true;
    }

    @Override
    public boolean registerDataAttend(String attendId, String attendDate, String attendDescription) {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(c.getTime());

        String attendDate1 = attendDate;

        attendDate1 = strDate.replace('-', '_').replace(':', '_').replace(' ','_');

        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("hc").child(attendId).child("veterinaryConsult").child(attendDate1).child("veterinaryAtention").setValue(attendDate + ": " + attendDescription);

        return true;
    }
}
